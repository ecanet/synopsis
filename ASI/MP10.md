# Administració de sistemes gestors de bases de dades

## M02-UF 1: introducció a les bases de dades

Atenció: aquesta Unitat Formativa és del Mòdul 2. La UF2 i la UF3 
del M02 es fan a primer.

En aquesta unitat es fa disseny de bases de dades relacionals.

* Relacions 1:N, N:N.
* Model Entitat Relació.
* Claus primàries, claus úniques, clas foranes.

## UF 1: llenguatges SQL: DCL i extensió procedimental

En aquesta unitat es fa desenvolupament de procediments enmagatzemats, es
veu com detectar els queries lents i com millorar-los, connexions externes
i màster-esclau

* Funcions PLPGSQL.
* Triggers a Postgres i a MySQL.
* Índexs, queries lents a Postgres i a MySQL. Materialized views.
* Connexions.
* ODBC.
* Model màster-esclau.

## UF 2: instal·lació i ajustament de SGBD corporatiu

En aquesta unitat es fa introducció a les BD NO-SQL, concretament al MongoDB.

* MongoDB + JSON.
* Find.
* Aggregation.
