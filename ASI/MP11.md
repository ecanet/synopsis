# Seguretat i alta disponibilitat

Aquest Mòdul M11 Seguretat i alta disponibilitat s’imparteix conjuntament amb
el mòdul M06 Administració de sistemes operatius. Els dos junts formen una
assignatura que podriem anomenar Sistemes Operatius i Seguretat.

Sovint la impartició primer de M06-ASO provoca que ens quedem curts de temps
per impartir els fascinats temes de seguretat, administració avançada i alta
disponibilitat que ens proposem.

Tots els continguts són impartits de manera pràctica amb exercicis reals als
hosts. Als alumnes sel’ls proporciona accés a materials més teòric i
descriptius com sóns els materials del IOC, com a complement teòric de la
formació pràctica que reben. [Materials
IOC](https://ioc.xtec.cat/educacio/recursos-materials-aprenentatge?id=92).

Tractant-se d’assignatures d’administració avançada s’exigeix a l’alumne que
sàpiga buscar fonts d’informació oficials i no manuals d’internet qualsevol.
Forma part de la documentació de referència:

* Fedora Documentation i els seus manuals de: Administration, Storage,
  Security, Networking i Virtualization.
* IBM Develpworks i els seus manuals de LPIC-1, LPIC-2 i LPIC-3
* Manuals d’utilització d’eines pròpies com: GIT, Docker i Pacemaker.

Podeu trobar el material del mòdul a la [web de
ASIX-M11](<https://sites.google.com/site/asixm11edt/>).

## UF 1: seguretat física, lògica i legislació

Aquesta unitat formativa es composa de dos nuclis formatius, el primer
absolutament pràctic tracta de temes de seguretat física i administració de
sistemes. El segon és totalment teòric i tracta els temes de la Legislació.

El primer nucli formatiu, treballa les següents àrees, de manera totalment
pràctica a l’ordinador. **Temes clau**:

* VLM. Volums Lògics
* RAID. Arrays de disc i combinació de RAID amb VLM.
* ACLs. Permisos avançats del sistema de fitxers i ACLs.
* Backups. Tipus de Backups. Gestió de Backups i automatització.
* Certificats digitals / Encriptació / Signat / Autenticació. 
* GP.: Encriptació de dades estàtiques. El model Web of trust.
* LUKS. Encriptació de particions.

El segon nucli d’activitat és totalment teòric. Es lliura a l’alumne
documentació a consultar com a pràctica personal. L’objecte d’estudi és la
Legislació referent a la privacitat de les dades i a la seguretat.

## UF 2: seguretat activa i accés remot

Mentre que a la UF1 s’han tractat tecnologies de seguretat (redundàcia, xifrat,
etc) de dades estàtiques (data at rest), aquesta segona unitat formativa es
centre en establir seguretat en els fluxos de dades (data in motion).

Abarca tant els aspectes de certificats digitals, xifrat i transmissió de dades
segures, com l’establiment de polítiques segures d’accés als recursos de xarxa.
Els **temes clau** tractats són:

* TCPWrappers. Polítiques de seguretat als serveis de xarxes.
* Kerberos. Implementació bàsics d’un reialmene i gestió de tikets.
* Tunels SSH. Utilització de túnels SSH per generar tràfic segur.
* VPN / IPSEC. Utilització de OpenVPN per a connexions segures.
* Model PKI de certificats digitals. OpenSSL a fons.

## UF 3: tallafocs i servidors intermediaris

El tema clau d’aquesta unitat formativa és la configuració de tallafocs. Es
treballa la creació de regles utilitzan iptables a fons. Seria desitjable
treballar també la capa superior que proporciona per exemple firewalld, però
sovint no hi ha temps.

Els conceptes de servidors intermediaris (proxy) sovint són obviats ja que es
treballen abastament al [Mòdul M08 Serveis de xarxa i
internet](<https://sites.google.com/site/asixm08edt/).

Així doncs els **temes clau de** la unitat formativa són:

* Firewalls usant iptables
* firewalld
* proxy

## UF 4: alta disponibilitat

L’última de les unitats formatives pateix la mateixa desgràcies que pateixen
totes les últimes uniats formatives, la manca de temps. Es per això que
transversalment al llarg de tot el curs (ja des del mòdul M06-ASO) s’ha
introduït els containers Docker, permetent arribar a final de curs amb uns bons
coneixements pràctics del seu funcionament.

Un segon aspecte treballat és la creació de Clústers d’Alta Disponibilitat
utilitzant Pacemaker Realitzant una pràctica de combinar dos o tres hosts
oferint serveis amb una ip flotant i un disc replicat. 

Un tercer aspecte, que queda pendent de treballar, és la utilització de serveis
d’alta disponibilitat i containers utilitzant proveïdors tipus Amazon Elastic
compute Cloud Google Cloud.

En resum, els **conceptes clau** són:

* Docker.
* Clusters amb Pacemaker.
* Cloud computing: Amazon / Google.
