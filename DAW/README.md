# Desenvolupament d’aplicacions web

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|-------|-----------
01     | Sistemes informàtics | Primer | 198 | 
02     | Bases de dades | Primer | 231 | 
03     | Programació | Primer i segon | 297 | 
04     | Llenguatges de marques i sistemes de gestió d’informació | Primer | 99 | 
05     | Entorns de desenvolupament | Segon | 66 | 
06     | Desenvolupament web en entorn client | Segon | 165 | 
07     | Desenvolupament web en entorn servidor | Segon | 165 | 
08     | Desplegament d’aplicacions web | Segon | 99 | 
09     | Disseny d’interfícies web | Segon | 99 | 
10     | Formació i orientació laboral | Primer | 99 | 
11     | Empresa i iniciativa emprenedora | Primer | 66 | 
12     | Projecte de desenvolupament d’aplicacions web | Segon | 120 | ⅀
13     | Formació en centres de treball | Segon | 317 | 

### Casos peculiars (FOL, FCT, etc.)

* MP10
* MP11
* MP12
* MP13
