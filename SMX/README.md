# Sistemes microinformàtics i xarxes

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|-------|-----------
01     | Muntatge i manteniment d’equips | Primer | 198 | 
02     | Sistemes operatius monolloc | Primer | 132 | 
03     | Aplicacions ofimàtiques | Primer | 165 | 
04     | Sistemes operatius en xarxa | Segon | 132 | 
05     | Xarxes locals | Primer | 165 | 
06     | Seguretat informàtica | Segon | 132 | 
07     | Serveis de xarxa | Segon | 165 | 
08     | Aplicacions web | Segon | 198 | 
09     | Formació i orientació laboral | Primer | 99 | 
10     | Empresa i iniciativa emprenedora | Primer | 66 | 
11     | Anglès tècnic | Segon | 99 | 
12     | Síntesi | Segon | 99 | ⅀
13     | Formació en centres de treball | Segon | 350 | 

## Mòduls redactats

* MP07 - Pablo García

### Casos peculiars (FOL, FCT, etc.)

* MP09
* MP10
* MP11
* MP12
* MP13
