# Desenvolupament d’aplicacions multiplataforma

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|-------|-----------
01     | Sistemes informàtics | Primer | 198 | 
02     | Bases de dades | Primer | 198 | 
03     | Programació | Primer/Segon | 295 | 
04     | Llenguatges de marques i sistemes de gestió d’informació | Primer | 99 | 
05     | Entorns de desenvolupament | Segon | 112 | 
06     | Accés a dades | Segon | 104 | 
07     | Desenvolupament d’interfícies | Segon | 112 | 
08     | Programació multimèdia i dispositius mòbils | Segon | 112 | 
09     | Programació de serveis i processos | Segon | 102 | 
10     | Sistemes de gestió empresarial | Segon | 66 | 
11     | Formació i orientació laboral | Primer | 99 | 
12     | Empresa i iniciativa emprenedora | Primer | 66 | 
13     | Projecte de desenvolupament d’aplicacions multiplataforma. | Segon | 120 | ⅀ 
14     | Formació en centres de treball | Segon | 317 | 

### Casos peculiars (FOL, FCT, etc.)

* MP11
* MP12
* MP13
* MP14
